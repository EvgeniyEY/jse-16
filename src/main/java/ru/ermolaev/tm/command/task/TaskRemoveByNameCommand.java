package ru.ermolaev.tm.command.task;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeTaskByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
