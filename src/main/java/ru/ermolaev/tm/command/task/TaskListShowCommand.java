package ru.ermolaev.tm.command.task;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Task;

import java.util.List;

public class TaskListShowCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().showAllTasks(userId);
        for (Task task: tasks) {
            System.out.println((tasks.indexOf(task) + 1) + ". " + task);
        }
        System.out.println("[COMPLETE]");
    }

}
