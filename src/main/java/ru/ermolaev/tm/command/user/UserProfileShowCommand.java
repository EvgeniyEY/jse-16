package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.User;

public class UserProfileShowCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "user-show-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show information about user account.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW USER PROFILE]");
        final User user = serviceLocator.getAuthenticationService().findCurrentUser();
        System.out.println(user);
        System.out.println("[OK]");
    }

}
