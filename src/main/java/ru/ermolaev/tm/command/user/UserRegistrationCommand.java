package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserRegistrationCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "registration";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Registration of new account.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().registration(login, password, email);
        System.out.println("[OK]");
    }

}
