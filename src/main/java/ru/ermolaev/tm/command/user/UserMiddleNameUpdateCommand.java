package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserMiddleNameUpdateCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "user-update-middle-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user middle name.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME:");
        final String newMiddleName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserMiddleName(newMiddleName);
        System.out.println("[OK]");
    }

}
