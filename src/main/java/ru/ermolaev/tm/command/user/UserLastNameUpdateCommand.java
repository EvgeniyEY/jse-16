package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserLastNameUpdateCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "user-update-last-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user last name.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME:");
        final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserLastName(newLastName);
        System.out.println("[OK]");
    }

}
