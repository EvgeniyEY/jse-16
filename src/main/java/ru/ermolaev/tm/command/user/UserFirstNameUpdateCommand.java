package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserFirstNameUpdateCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "user-update-first-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user first name.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME:");
        final String newFirstName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserFirstName(newFirstName);
        System.out.println("[OK]");
    }

}
