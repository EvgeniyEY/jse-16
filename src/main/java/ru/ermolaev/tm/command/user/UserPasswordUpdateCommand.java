package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserPasswordUpdateCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "user-update-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user password.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updatePassword(newPassword);
        System.out.println("[PASSWORD CHANGED]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

}
