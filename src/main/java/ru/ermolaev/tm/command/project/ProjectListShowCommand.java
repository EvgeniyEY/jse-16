package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;

import java.util.List;

public class ProjectListShowCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECTS LIST]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final List<Project> projects = serviceLocator.getProjectService().showAllProjects(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[COMPLETE]");
    }

}
