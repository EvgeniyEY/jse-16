package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeProjectById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
