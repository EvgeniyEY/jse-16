package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

}
