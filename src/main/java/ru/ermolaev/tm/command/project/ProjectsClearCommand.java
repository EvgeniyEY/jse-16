package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;

public class ProjectsClearCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        serviceLocator.getProjectService().removeAllProjects(userId);
        System.out.println("[COMPLETE]");
    }

}
