package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = serviceLocator.getProjectService().removeProjectByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
