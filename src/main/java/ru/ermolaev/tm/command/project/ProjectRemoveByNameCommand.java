package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeProjectByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
