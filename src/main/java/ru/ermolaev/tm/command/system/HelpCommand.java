package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command: commands) {
            System.out.println(command.commandName()
                    + (command.arg() == null ? ": " : ", " + command.arg() + ": ")
                    + command.description());
        }
    }

}
