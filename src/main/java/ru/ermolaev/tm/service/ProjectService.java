package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.exception.empty.EmptyIdException;
import ru.ermolaev.tm.exception.empty.EmptyUserIdException;
import ru.ermolaev.tm.exception.incorrect.IncorrectIndexException;
import ru.ermolaev.tm.exception.empty.EmptyNameException;
import ru.ermolaev.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void createProject(final String userId, final String name) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void createProject(final String userId, final String name, final String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void addProject(final String userId, final Project project) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public List<Project> showAllProjects(final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeProject(final String userId, final Project project) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public void removeAllProjects(final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    public Project findProjectById(final String userId, final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project findProjectByIndex(final String userId, final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public Project findProjectByName(final String userId, final String name) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project updateProjectById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findProjectById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findProjectByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectById(final String userId, final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project removeProjectByName(final String userId, final String name) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

}
