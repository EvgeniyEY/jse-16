package ru.ermolaev.tm;

import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.exception.AbstractException;

public class Application {

    public static void main(final String[] args) throws AbstractException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
