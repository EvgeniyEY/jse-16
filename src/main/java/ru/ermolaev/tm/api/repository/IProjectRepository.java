package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    List<Project> findAll(String userId);

    void remove(String userId, Project project);

    void clear(String userId);

    Project findById(String userId, String id) throws AbstractException;

    Project findByIndex(String userId, Integer index) throws AbstractException;

    Project findByName(String userId, String name) throws AbstractException;

    Project removeById(String userId, String id) throws AbstractException;

    Project removeByIndex(String userId, Integer index) throws AbstractException;

    Project removeByName(String userId, String name) throws AbstractException;

}
