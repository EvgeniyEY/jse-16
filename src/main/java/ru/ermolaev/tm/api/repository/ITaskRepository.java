package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    List<Task> findAll(String userId);

    void remove(String userId, Task task);

    void clear(String userId);

    Task findById(String userId, String id) throws AbstractException;

    Task findByIndex(String userId, Integer index) throws AbstractException;

    Task findByName(String userId, String name) throws AbstractException;

    Task removeById(String userId, String id) throws AbstractException;

    Task removeByIndex(String userId, Integer index) throws AbstractException;

    Task removeByName(String userId, String name) throws AbstractException;

}
