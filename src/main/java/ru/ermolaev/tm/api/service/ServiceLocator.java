package ru.ermolaev.tm.api.service;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthenticationService getAuthenticationService();

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

}
