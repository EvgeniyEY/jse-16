package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.User;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User updatePassword(String userId, String newPassword) throws AbstractException;

    User findById(String id) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    User removeUser(User user);

    User removeById(String id) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User updateUserFirstName(String userId, String newFirstName) throws AbstractException;

    User updateUserMiddleName(String userId, String newMiddleName) throws AbstractException;

    User updateUserLastName(String userId, String newLastName) throws AbstractException;

    User updateUserEmail(String userId, String newEmail) throws AbstractException;

    User lockUserByLogin(String login) throws AbstractException;

    User unlockUserByLogin(String login) throws AbstractException;

}
