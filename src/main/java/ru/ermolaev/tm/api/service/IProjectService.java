package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void createProject(String userId, String name) throws AbstractException;

    void createProject(String userId, String name, String description) throws AbstractException;

    void addProject(String userId, Project project) throws AbstractException;

    List<Project> showAllProjects(String userId) throws AbstractException;

    void removeProject(String userId, Project project) throws AbstractException;

    void removeAllProjects(String userId) throws AbstractException;

    Project findProjectById(String userId, String id) throws AbstractException;

    Project findProjectByIndex(String userId, Integer index) throws AbstractException;

    Project findProjectByName(String userId, String name) throws AbstractException;

    Project updateProjectById(String userId, String id, String name, String description) throws AbstractException;

    Project updateProjectByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Project removeProjectById(String userId, String id) throws AbstractException;

    Project removeProjectByIndex(String userId, Integer index) throws AbstractException;

    Project removeProjectByName(String userId, String name) throws AbstractException;

}
