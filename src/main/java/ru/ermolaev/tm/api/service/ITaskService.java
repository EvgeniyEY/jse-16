package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void createTask(String userId, String name) throws AbstractException;

    void createTask(String userId, String name, String description) throws AbstractException;

    void addTask(String userId, Task task) throws AbstractException;

    List<Task> showAllTasks(String userId) throws AbstractException;

    void removeTask(String userId, Task task) throws AbstractException;

    void removeAllTasks(String userId) throws AbstractException;

    Task findTaskById(String userId, String id) throws AbstractException;

    Task findTaskByIndex(String userId, Integer index) throws AbstractException;

    Task findTaskByName(String userId, String name) throws AbstractException;

    Task updateTaskById(String userId, String id, String name, String description) throws AbstractException;

    Task updateTaskByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task removeTaskById(String userId, String id) throws AbstractException;

    Task removeTaskByIndex(String userId, Integer index) throws AbstractException;

    Task removeTaskByName(String userId, String name) throws AbstractException;

}
