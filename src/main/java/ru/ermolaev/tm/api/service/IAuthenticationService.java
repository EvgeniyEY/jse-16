package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.model.User;

public interface IAuthenticationService {

    String getUserId() throws AbstractException;

    boolean isAuthenticated();

    void login(String login, String password) throws AbstractException;

    void logout();

    void registration(String login, String password, String email) throws AbstractException;

    void updatePassword(String newPassword) throws AbstractException;

    User findCurrentUser() throws AbstractException;

    void updateUserFirstName(String newFirstName) throws AbstractException;

    void updateUserMiddleName(String newMiddleName) throws AbstractException;

    void updateUserLastName(String newLastName) throws AbstractException;

    void updateUserEmail(String newEmail) throws AbstractException;

    void checkRole(Role[] roles) throws AbstractException;

}
