package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownLoginException extends AbstractException {

    public UnknownLoginException() {
        super("Error! This login does not exist.");
    }

    public UnknownLoginException(final String login) {
        super("Error! This login [" + login + "] does not exist.");
    }

}
