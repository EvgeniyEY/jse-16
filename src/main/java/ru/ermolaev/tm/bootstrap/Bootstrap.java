package ru.ermolaev.tm.bootstrap;

import ru.ermolaev.tm.api.repository.*;
import ru.ermolaev.tm.api.service.*;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.AbstractException;
import ru.ermolaev.tm.exception.unknown.UnknownArgumentException;
import ru.ermolaev.tm.exception.unknown.UnknownCommandException;
import ru.ermolaev.tm.exception.empty.EmptyCommandException;
import ru.ermolaev.tm.repository.*;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.service.*;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initCommands(commandService.getCommandList());
        try {
            initUsers();

            initTask("user","user-task1", "task1");
            initTask("user","user-task2", "task2");
            initTask("user","user-task3", "task3");
            initTask("user","user-task4", "task4");
            initTask("user","user-task5", "task5");

            initTask("admin","admin-task1", "task1");
            initTask("admin","admin-task2", "task2");
            initTask("admin","admin-task3", "task3");
            initTask("admin","admin-task4", "task4");
            initTask("admin","admin-task5", "task5");

            initProject("user","user-project1", "project1");
            initProject("user","user-project2", "project2");
            initProject("user","user-project3", "project3");
            initProject("user","user-project4", "project4");
            initProject("user","user-project5", "project5");

            initProject("admin","admin-project1", "project1");
            initProject("admin","admin-project2", "project2");
            initProject("admin","admin-project3", "project3");
            initProject("admin","admin-project4", "project4");
            initProject("admin","admin-project5", "project5");
        } catch (AbstractException e) {
            e.printStackTrace();
        }
    }

    private void initTask(final String login, final String taskName, final String taskDescription)
            throws AbstractException {
        taskService.createTask(userService.findByLogin(login).getId(),taskName,taskDescription);
    }

    private void initProject(final String login, final String projectName, final String projectDescription)
            throws AbstractException {
        projectService.createProject(userService.findByLogin(login).getId(),projectName,projectDescription);
    }

    private void initCommands(List<AbstractCommand> commandList) {
        for (AbstractCommand command: commandList) init(command);
    }

    private void initUsers() throws AbstractException {
        userService.create("user", "user", "user@test.ru");
        userService.create("test", "test", "test@test.ru");
        userService.create("guest", "guest", "guest@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("root", "root", Role.ADMIN);
    }

    private void init(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    public void run(final String[] args) throws AbstractException {
        System.out.println("Welcome to task manager");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) throws AbstractException {
        if (arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    public void parseCommand(final String cmd) throws AbstractException {
        if (cmd.isEmpty()) throw new EmptyCommandException();
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authenticationService.checkRole(command.roles());
        command.execute();
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

}
